package br.com.albovieira.sisdoapp.Constante;

/**
 * Created by albov on 29/08/2015.
 */
public class UsuarioConst {

    public static final String nomeTabela = "usuario";

    public static final String id = "_id";
    public static final String nome = "nomeUsuario";
    public static final String email = "email";
    public static final String senha = "senha";
    public static final String perfil = "perfil";

}
